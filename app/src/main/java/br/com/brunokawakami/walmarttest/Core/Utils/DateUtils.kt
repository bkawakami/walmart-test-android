package br.com.brunokawakami.walmarttest.Core.Utils

import com.orhanobut.logger.Logger

import java.sql.Timestamp
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.HashMap

/**
 * Created by omicron on 23/04/15.
 */
object DateUtils {

    var dateFormatDayMonthYear = SimpleDateFormat("dd/MM/yyyy")
    var dateFormatHourMinute = SimpleDateFormat("HH:mm")
    var dateFormatString = SimpleDateFormat("ss")
    var dateFormatHourMinuteAMPM = SimpleDateFormat("HH:mm aa")
    var dateFormatWithTimeZone = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
    var dateFormatYearMonthDay = SimpleDateFormat("yyyy-MM-dd")
    var dateFormatDateTime = SimpleDateFormat("dd/MM/yyyy HH:mm")
    var dateFormatDayMothYearHourMinuteSecond = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var dateFormatDayMothYearTexto = SimpleDateFormat("d MMM yy")
    var dateFormatDayMothTexto = SimpleDateFormat("d MMM")
    var dateFormatYearTexto = SimpleDateFormat("yyyy")
    var dateFormatDayMothYearHourMinText = SimpleDateFormat("d MMM yy HH:mm")
    var dateFormatDayMothCommaYearTexto = SimpleDateFormat("d MMM yyyy")

    fun convertStringToDate(dateStr: String): Date? {

        val determineFormat = determineDateFormat(dateStr)

        val formatDate = SimpleDateFormat(determineFormat)

        try {
            return formatDate.parse(dateStr)
        } catch (e: ParseException) {
            Logger.e(e.message)
            return null
        }

    }

    fun determineDateFormat(dateString: String): String? {
        for (regexp in DATE_FORMAT_REGEXPS.keys) {
            if (dateString.toLowerCase().matches(regexp.toRegex())) {
                return DATE_FORMAT_REGEXPS[regexp]
            }
        }
        return null
    }


    private val DATE_FORMAT_REGEXPS = object : HashMap<String, String>() {
        init {
            put("^\\d{8}$", "yyyyMMdd")
            put("^\\d{1,2}-\\d{1,2}-\\d{4}$", "dd-MM-yyyy")
            put("^\\d{1,1}-\\d{1,1}-\\d{4}$", "d-M-yyyy")
            put("^\\d{4}-\\d{1,2}-\\d{1,2}$", "yyyy-MM-dd")
            put("^\\d{1,2}/\\d{1,2}/\\d{4}$", "dd/MM/yyyy")
            put("^\\d{4}/\\d{1,2}/\\d{1,2}$", "yyyy/MM/dd")
            put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}$", "dd MMM yyyy")
            put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}$", "dd MMMM yyyy")
            put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}$", "d MMM, yyyy")
            put("^\\d{12}$", "yyyyMMddHHmm")
            put("^\\d{8}\\s\\d{4}$", "yyyyMMdd HHmm")
            put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}$", "dd-MM-yyyy HH:mm")
            put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy-MM-dd HH:mm")
            put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}$", "dd/MM/yyyy HH:mm")
            put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy/MM/dd HH:mm")
            put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMM yyyy HH:mm")
            put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMMM yyyy HH:mm")
            put("^\\d{14}$", "yyyyMMddHHmmss")
            put("^\\d{8}\\s\\d{6}$", "yyyyMMdd HHmmss")
            put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd-MM-yyyy HH:mm:ss")
            put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy-MM-dd HH:mm:ss")
            put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd/MM/yyyy HH:mm:ss")
            put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy/MM/dd HH:mm:ss")
            put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMM yyyy HH:mm:ss")
            put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMMM yyyy HH:mm:ss")
            put("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[+-]\\d{4}$", "yyyy-MM-dd'T'HH:mm:ssZ")

        }
    }

    // get today and clear time of day
    // ! clear would not reset the hour of day !
    val firstDayCurrentWeek: String
        get() {
            val cal = Calendar.getInstance()
            cal.set(Calendar.HOUR_OF_DAY, 0)
            cal.clear(Calendar.MINUTE)
            cal.clear(Calendar.SECOND)
            cal.clear(Calendar.MILLISECOND)

            cal.set(Calendar.DAY_OF_WEEK, cal.firstDayOfWeek)

            val currentWeek = cal.time

            val firstDay = Timestamp(currentWeek.time)

            return (firstDay.time / 1000).toInt().toString()
        }

    // get today and clear time of day
    // ! clear would not reset the hour of day !
    val lastDayCurrentWeek: String
        get() {
            val cal = Calendar.getInstance()
            cal.set(Calendar.HOUR_OF_DAY, 0)
            cal.clear(Calendar.MINUTE)
            cal.clear(Calendar.SECOND)
            cal.clear(Calendar.MILLISECOND)

            cal.set(Calendar.DAY_OF_WEEK, cal.firstDayOfWeek)

            cal.add(Calendar.DAY_OF_WEEK, 6)

            val currentWeek = cal.time

            val lastDay = Timestamp(currentWeek.time)

            return (lastDay.time / 1000).toInt().toString()
        }

    val currentDateStr: String
        get() {
            val c = Calendar.getInstance()

            val df = SimpleDateFormat("dd MMM yyyy")
            val formattedDate = df.format(c.time)

            return formattedDate

        }

    fun formatStringToFormat(stringDate: String, toFormat: SimpleDateFormat): String {
        try {
            val determineFormat = determineDateFormat(stringDate)
            val originalFormat = SimpleDateFormat(determineFormat)
            val date = originalFormat.parse(stringDate)
            return toFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
            return ""
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }

    }

    fun formatDateToFormat(date: Date, toFormat: SimpleDateFormat): String {

        return toFormat.format(date)

    }

    fun formatStringToTimestamp(stringDate: String): Long? {
        val determineFormat = determineDateFormat(stringDate)
        val originalFormat = SimpleDateFormat(determineFormat)

        try {
            return originalFormat.parse(stringDate).time
        } catch (e: Exception) {
            e.printStackTrace()
            Logger.e(e.message)
            return null
        }

    }

}
