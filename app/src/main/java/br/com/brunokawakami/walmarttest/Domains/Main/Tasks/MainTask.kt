package br.com.brunokawakami.walmarttest.Domains.Main.Tasks

import android.app.Activity
import android.os.AsyncTask
import br.com.brunokawakami.walmarttest.Core.Api.Base.RestAdapter
import br.com.brunokawakami.walmarttest.Core.Api.Receivers.LoginReceiver
import br.com.brunokawakami.walmarttest.Core.Api.Requests.LoginRequest
import br.com.brunokawakami.walmarttest.Core.Api.Requests.MeRequest
import br.com.brunokawakami.walmarttest.Core.App.GlobalConfig
import br.com.brunokawakami.walmarttest.Core.Storage.Repositories.LoginRepository
import com.orhanobut.logger.Logger

/**
 * Created by bruno on 6/26/16.
 */
object MainTask {
    fun doLogin(
            email: String,
            password: String,
            doneExecute: (loginReceiver: LoginReceiver?) -> Unit
    ): AsyncTask<Void, Void, LoginReceiver?> {

        return object: AsyncTask<Void,Void,LoginReceiver?>(){
            override fun doInBackground(vararg p0: Void?): LoginReceiver? {
                try {
                    val loginRequest = RestAdapter.getBaseAdapter().create(LoginRequest::class.java)
                    return loginRequest.postGeneralLogin(
                            GlobalConfig.getClientId(),
                            GlobalConfig.getClientSecret(),
                            "password",
                            email,
                            password
                    ).execute().body()
                } catch(e: Exception) {
                    Logger.e(e.message)
                    e.printStackTrace()
                    return null
                }
            }

            override fun onPostExecute(result: LoginReceiver?) {
                doneExecute(result)
            }
        }

    }

    fun postToken(activity: Activity, done: () -> Unit): AsyncTask<Void, Void, Unit> {
        return object: AsyncTask<Void,Void,Unit>(){
            override fun doInBackground(vararg p0: Void?) {
                try {
                    val loginRecord = LoginRepository.getCurrentLogin()
                    val loginRequest = RestAdapter.getOauthAdapter(activity).create(MeRequest::class.java)
                    loginRequest.token(loginRecord!!.fcmToken).execute()
                } catch(e: Exception) {
                    Logger.e(e.message)
                    e.printStackTrace()
                }
            }

            override fun onPostExecute(result: Unit) {
                done()
            }
        }

    }

}