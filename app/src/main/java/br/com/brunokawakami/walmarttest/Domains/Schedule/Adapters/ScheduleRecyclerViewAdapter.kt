package br.com.brunokawakami.walmarttest.Domains.Schedule.Adapters

import android.app.Activity
import android.content.Intent
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import br.com.brunokawakami.walmarttest.Core.Storage.Entities.ScheduleEntity
import br.com.brunokawakami.walmarttest.Core.Utils.DateUtils
import br.com.brunokawakami.walmarttest.Domains.Schedule.Activities.AddScheduleActivity

import br.com.brunokawakami.walmarttest.R
import com.afollestad.materialdialogs.MaterialDialog

class ScheduleRecyclerViewAdapter(
        private val mValues: List<ScheduleEntity>,
        private val onClick: (ScheduleEntity) -> Unit
) :
        RecyclerView.Adapter<ScheduleRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_schedule_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mItem = mValues[position]
        holder.mDateView.text = DateUtils.formatDateToFormat(mValues[position].begin!!, DateUtils.dateFormatDateTime)
        holder.mContentView.text = mValues[position].title
        holder.cardView.setOnClickListener{
            onClick(mValues[position])
        }
    }

    override fun getItemCount(): Int {
        return mValues.size
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mDateView: TextView
        val mContentView: TextView
        val cardView: CardView
        var mItem: ScheduleEntity? = null

        init {
            mDateView = mView.findViewById(R.id.textViewDate) as TextView
            mContentView = mView.findViewById(R.id.textViewTitle) as TextView
            cardView = mView.findViewById(R.id.cardView) as CardView
        }

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }
}
