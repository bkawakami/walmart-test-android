package br.com.brunokawakami.walmarttest.Core.Api.Receivers

import com.google.gson.annotations.SerializedName

/**
 * Created by bruno on 6/24/16.
 */
class BaseScheduleReceiver {
    var status: String? = null

    @SerializedName("return")
    var returnData: List<ScheduleReceiver>? = null
}