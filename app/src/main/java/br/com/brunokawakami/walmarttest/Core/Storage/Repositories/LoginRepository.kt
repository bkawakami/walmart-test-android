package br.com.brunokawakami.walmarttest.Core.Storage.Repositories

import br.com.brunokawakami.walmarttest.Core.Storage.Entities.LoginEntity
import com.orm.query.Select

/**
 * Created by bruno on 6/24/16.
 */
object LoginRepository {
    fun getCurrentLogin(): LoginEntity?{
        return Select.from(LoginEntity::class.java).orderBy("id DESC").first()
    }

    fun saveLogin(accessToken: String?, refreshToken: String?, token: String?): LoginEntity?{
        var record = getCurrentLogin()
        if (record == null) {
            record = LoginEntity(
                accessToken,
                refreshToken,
                token
            )
            record.save()
        } else {

            if (token != null) {
                record.fcmToken = token
            }

            if (accessToken != null) {
                record.accessToken = accessToken
            }

            if (refreshToken != null) {
                record.refreshToken = refreshToken
            }

            record.save()
        }
        return record
    }
}