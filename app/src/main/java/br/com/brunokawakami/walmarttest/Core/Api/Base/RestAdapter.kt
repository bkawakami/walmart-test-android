package br.com.brunokawakami.walmarttest.Core.Api.Base

import android.app.Activity
import android.content.Intent
import br.com.brunokawakami.walmarttest.Core.Api.Receivers.LoginReceiver
import br.com.brunokawakami.walmarttest.Core.Api.Requests.LoginRequest
import br.com.brunokawakami.walmarttest.Core.App.GlobalConfig
import br.com.brunokawakami.walmarttest.Core.Storage.Repositories.LoginRepository
import br.com.brunokawakami.walmarttest.Domains.Main.Activities.MainActivity
import com.orhanobut.logger.Logger
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by bruno on 6/24/16.
 */
object RestAdapter {
    fun getBaseAdapter(): Retrofit {

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        var httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(interceptor)

        val restAdapter = Retrofit.Builder().baseUrl(GlobalConfig.getUrl())
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        return restAdapter
    }


    fun getOauthAdapter(activity: Activity): Retrofit {

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        var httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor({ chain ->
            val login = LoginRepository.getCurrentLogin()
            var request = chain.request().newBuilder().addHeader("Authorization", "Bearer "+login!!.accessToken).build()
            var response = chain.proceed(request)
            if (response.code() == 401) {
                val loginReceiver = getToken(activity)
                if (loginReceiver != null) {
                    login.accessToken = loginReceiver.accessToken
                    login.refreshToken = loginReceiver.refreshToken
                    login.save()
                    request = chain.request().newBuilder().addHeader("Authorization", "Bearer "+loginReceiver.accessToken).build()
                    response = chain.proceed(request)
                }
            }
            response
        })
        httpClient.addInterceptor(interceptor)

        val restAdapter = Retrofit.Builder().baseUrl(GlobalConfig.getUrl())
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        return restAdapter
    }

    private fun getToken(activity: Activity): LoginReceiver?{
        try {
            val login = LoginRepository.getCurrentLogin()
            val request = RestAdapter.getBaseAdapter().create(LoginRequest::class.java)
            val refreshToken = request.postRefreshToken(
                    GlobalConfig.getClientId(),
                    GlobalConfig.getClientSecret(),
                    "refresh_token",
                    login!!.refreshToken!!

            ).execute()
            if (!refreshToken.isSuccessful) {
                logout(activity)
                Logger.e(refreshToken.code().toString())
                Logger.e(refreshToken.errorBody().string())
            }
            return refreshToken.body()
        } catch(e: Exception) {
            Logger.e(e.message)
            logout(activity)
            return null
        }
    }

    private fun logout(activity: Activity){
        //LoginRepository.truncate()
        val intent = Intent(activity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        activity.startActivity(intent)
        activity.finish()
    }
}