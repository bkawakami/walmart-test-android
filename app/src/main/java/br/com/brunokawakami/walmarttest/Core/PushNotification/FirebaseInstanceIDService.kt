package br.com.brunokawakami.walmarttest.Core.PushNotification

/**
 * Created by bruno on 6/28/16.
 */
import android.util.Log
import br.com.brunokawakami.walmarttest.Core.Storage.Repositories.LoginRepository

import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService


class FirebaseInstanceIDService : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {

        val refreshedToken = FirebaseInstanceId.getInstance().token

        sendRegistrationToServer(refreshedToken)
    }

    private fun sendRegistrationToServer(token: String?) {
        LoginRepository.saveLogin(null, null, token)
    }


}
