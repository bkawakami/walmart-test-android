package br.com.brunokawakami.walmarttest.Core.Api.Requests

import br.com.brunokawakami.walmarttest.Core.Api.Receivers.LoginReceiver
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * Created by bruno on 6/24/16.
 */
interface LoginRequest {
    @FormUrlEncoded
    @POST("oauth/access_token")
    fun postGeneralLogin(
            @Field("client_id") clientId: String,
            @Field("client_secret") clientSecret: String,
            @Field("grant_type") grantType: String,
            @Field("username") username: String,
            @Field("password") password: String
    ): Call<LoginReceiver>

    @FormUrlEncoded
    @POST("oauth/access_token")
    fun postRefreshToken(
            @Field("client_id") clientId: String,
            @Field("client_secret") clientSecret: String,
            @Field("grant_type") grantType: String,
            @Field("refresh_token") username: String
    ): Call<LoginReceiver>

    @FormUrlEncoded
    @POST("oauth/access_token")
    fun postFacebookLogin(
            @Field("client_id") clientId: String,
            @Field("client_secret") clientSecret: String,
            @Field("grant_type") grantType: String,
            @Field("facebook_id") username: String
    ): Call<LoginReceiver>
}