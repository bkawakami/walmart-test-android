package br.com.brunokawakami.walmarttest.Core.App

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import com.facebook.stetho.Stetho
import com.orm.SugarContext

/**
 * Created by bruno on 6/27/16.
 */
class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        SugarContext.init(this)
        Stetho.initializeWithDefaults(this);
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(applicationContext)
    }
}