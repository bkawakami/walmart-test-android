package br.com.brunokawakami.walmarttest.Core.Storage.Entities

import com.orm.SugarRecord
import java.util.*

/**
 * Created by bruno on 6/24/16.
 */
class ScheduleEntity: SugarRecord {

    var serverId: Int? = null
    var title: String? = null
    var content: String? = null
    var begin: Date? = null
    var timeToRemember: Int? = null

    constructor(){}

    constructor(
            serverId: Int,
            title: String?,
            content: String?,
            begin: Date?,
            timeToRemember: Int?
    ){
        this.serverId = serverId
        this.title = title
        this.content = content
        this.begin = begin
        this.timeToRemember = timeToRemember
    }

}