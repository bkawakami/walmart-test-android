package br.com.brunokawakami.walmarttest.Core.Api.Receivers

import com.google.gson.annotations.SerializedName

/**
 * Created by bruno on 6/24/16.
 */
class ScheduleReceiver {
    var id: Int? = null
    var begin: String? = null
    var title: String? = null
    var content: String? = null
    @SerializedName("time_to_remember")
    var timeToRemember: Int? = null
}