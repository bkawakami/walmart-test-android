package br.com.brunokawakami.walmarttest.Domains.Schedule.Fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.brunokawakami.walmarttest.Core.Utils.DialogUtils
import br.com.brunokawakami.walmarttest.Domains.Schedule.Activities.AddScheduleActivity
import br.com.brunokawakami.walmarttest.Domains.Schedule.Adapters.ScheduleRecyclerViewAdapter

import br.com.brunokawakami.walmarttest.R
import br.com.brunokawakami.walmarttest.Domains.Schedule.Tasks.ScheduleTasks
import com.afollestad.materialdialogs.MaterialDialog


class ScheduleFragment : Fragment() {

    var recyclerViewSchedules: RecyclerView? = null
    var swipeRefreshLayout: SwipeRefreshLayout? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_schedule, container, false)
        recyclerViewSchedules = view.findViewById(R.id.recyclerViewSchedules) as RecyclerView
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout) as SwipeRefreshLayout
        swipeRefreshLayout?.setOnRefreshListener {
            updateSchedule()
        }
        swipeRefreshLayout?.isRefreshing = true
        updateSchedule()
        return view
    }

    private fun updateSchedule(){
        ScheduleTasks.getSchedules(activity) { listSchedule ->
            if (listSchedule!=null) {
                recyclerViewSchedules?.adapter =
                        ScheduleRecyclerViewAdapter(listSchedule) { schedule ->
                            MaterialDialog.Builder(activity)
                                    .title("Ações")
                                    .items("Editar","Excluír")
                                    .itemsCallback { materialDialog, view, i, charSequence ->
                                        when(i){
                                            0->{
                                                val intent = Intent(activity, AddScheduleActivity::class.java)
                                                intent.putExtra("serverId", schedule.serverId)
                                                activity.startActivity(intent)
                                            }
                                            1->{
                                                val mDiagog = DialogUtils.progress(activity)
                                                ScheduleTasks.deleteSchedule(activity, schedule.serverId!!){
                                                    swipeRefreshLayout?.isRefreshing = true
                                                    updateSchedule()
                                                    mDiagog.dismiss()
                                                }.execute()
                                            }
                                        }
                                    }
                                    .show()
                        }
            }
            swipeRefreshLayout?.isRefreshing = false
        }.execute()
    }

    override fun onResume() {
        super.onResume()
        swipeRefreshLayout?.isRefreshing = true
        updateSchedule()
    }

}
