package br.com.brunokawakami.walmarttest.Core.Storage.Entities

import com.orm.SugarRecord

/**
 * Created by bruno on 6/24/16.
 */
class LoginEntity: SugarRecord {

    var accessToken: String? = null
    var refreshToken: String? = null
    var fcmToken: String? = null

    constructor(){}

    constructor(
            accessToken: String?,
            refreshToken: String?,
            fcmToken: String?
    ){
        this.accessToken = accessToken
        this.refreshToken = refreshToken
        this.fcmToken = fcmToken
    }

}