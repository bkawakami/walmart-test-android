package br.com.brunokawakami.walmarttest.Core.App

import br.com.brunokawakami.walmarttest.BuildConfig

/**
 * Created by bruno on 6/24/16.
 */
object GlobalConfig {
    val baseUrlProduction = "http://walmart.brunokawakami.com.br/api/"
    val baseUrlBeta = "http://walmart.brunokawakami.com.br/api/"
    val clientIdBeta: String = "7ed147dfbd747b6b277f65ad85fcc2e55f3bf5c2"
    val clientIdProd: String = "7ed147dfbd747b6b277f65ad85fcc2e55f3bf5c2"
    val clientSecretBeta: String = "89fd668d47f2084ab01ec04bbeeba16685cdffc8"
    val clientSecretProd: String = "89fd668d47f2084ab01ec04bbeeba16685cdffc8"
    val version: String = "v1/"
    fun getUrl():String = if (!BuildConfig.DEBUG) baseUrlProduction+version else baseUrlBeta+version
    fun getClientId():String = if (!BuildConfig.DEBUG) clientIdProd else clientIdBeta
    fun getClientSecret():String = if (!BuildConfig.DEBUG) clientSecretProd else clientSecretBeta
}