package br.com.brunokawakami.walmarttest.Core.Api.Requests

import br.com.brunokawakami.walmarttest.Core.Api.Receivers.BaseScheduleReceiver
import br.com.brunokawakami.walmarttest.Core.Api.Receivers.InsertScheduleReceiver
import br.com.brunokawakami.walmarttest.Core.Api.Receivers.LoginReceiver
import br.com.brunokawakami.walmarttest.Core.Api.Receivers.ScheduleReceiver
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by bruno on 6/24/16.
 */
interface ScheduleRequest {
    @FormUrlEncoded
    @POST("schedule/insert")
    fun insert(
            @Field("begin") begin: String?,
            @Field("title") title: String?,
            @Field("content") content: String?,
            @Field("time_to_remember") timeToRemember: Int?
    ): Call<InsertScheduleReceiver>

    @FormUrlEncoded
    @POST("schedule/update/{id}")
    fun update(
            @Path("id") id: Int,
            @Field("begin") begin: String?,
            @Field("title") title: String?,
            @Field("content") content: String?,
            @Field("time_to_remember") timeToRemember: Int?
    ): Call<InsertScheduleReceiver>

    @POST("schedule/delete/{id}")
    fun delete(
            @Path("id") id: Int
    ): Call<InsertScheduleReceiver>

    @GET("schedule/all")
    fun all(): Call<BaseScheduleReceiver>

    @GET("schedule/search/{search}")
    fun search(
            @Path("search") search: String
    ): Call<BaseScheduleReceiver>
}