package br.com.brunokawakami.walmarttest.Core.Utils

import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.util.Base64
import com.orhanobut.logger.Logger
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.*

/**
 * Created by bruno on 4/1/16.
 */

object StoreUtils {

    fun saveFromBitmap(ctx: Context, image: Bitmap): File? {
        val cw = ContextWrapper(ctx)

        val directory = cw.getDir("pictures", Context.MODE_PRIVATE)

        val finalBitmap = image

        val fname = UUID.randomUUID().toString() + ".jpg"
        val file = File(directory, fname)

        if (file.exists()) {
            file.delete()
        }
        try {
            val out = FileOutputStream(file)
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
            //out.flush();
            out.close()
            val avatar = File(directory, fname)
            return avatar
        } catch (e: Exception) {
            e.printStackTrace()
            Logger.e(e.message)
            return null
        }

    }

    fun getResizedBitmap(bm:Bitmap, newHeight: Int, newWidth:Int):Bitmap {

        var matrix = Matrix();

        var width = bm.width;

        var height = bm.height;

        var scaleWidth: Float
        var scaleHeight: Float
        if (height >  width) {
            scaleWidth = (newWidth.toFloat()) / height

            scaleHeight = (newHeight.toFloat()) / height

            matrix.postScale(scaleHeight, scaleHeight)
        } else {
            scaleWidth = (newWidth.toFloat()) / width

            scaleHeight = (newHeight.toFloat()) / width

            matrix.postScale(scaleWidth, scaleWidth)

        }

        // RECREATE THE NEW BITMAP

        var resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false)

        return resizedBitmap;

    }


    fun fileToBase64(filePath: String): String? {
        val bm = BitmapFactory.decodeFile(filePath)
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b = baos.toByteArray()

        val encodeImage: String = Base64.encodeToString(b, Base64.DEFAULT)

        return encodeImage
    }

    
}
