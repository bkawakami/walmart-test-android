package br.com.brunokawakami.walmarttest.Domains.Main.Activities

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.FragmentActivity
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import br.com.brunokawakami.walmarttest.Core.Storage.Repositories.LoginRepository
import br.com.brunokawakami.walmarttest.Core.Utils.ConnectionUtils
import br.com.brunokawakami.walmarttest.Core.Utils.DialogUtils
import br.com.brunokawakami.walmarttest.Core.Utils.SnackbarUtils
import br.com.brunokawakami.walmarttest.Domains.Main.Tasks.MainTask
import br.com.brunokawakami.walmarttest.Domains.Schedule.Activities.ScheduleActivity
import br.com.brunokawakami.walmarttest.R

import mehdi.sakout.fancybuttons.FancyButton

class MainActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        val imageViewBrand = findViewById(R.id.imageViewBrand) as ImageView
        val animation = ObjectAnimator.ofFloat(imageViewBrand, 'y'.toString(), 20.toFloat())
        val linearLayoutLogin = findViewById(R.id.linearLayoutLogin) as LinearLayout
        val progressBar = findViewById(R.id.progressBar) as ProgressBar
        val fancyButtonLogin = findViewById(R.id.fancyButtonLogin) as FancyButton

        fancyButtonLogin.setOnClickListener {
            val materialDialog = DialogUtils.progress(this)
            val editTextEmail = findViewById(R.id.editTextEmail) as EditText
            val editTextPassword = findViewById(R.id.editTextPassword) as EditText
            MainTask.doLogin(editTextEmail.text.toString(), editTextPassword.text.toString()) { loginReceiver ->
                materialDialog.dismiss()
                if (loginReceiver != null) {
                    LoginRepository.saveLogin(loginReceiver.accessToken, loginReceiver.refreshToken, null)
                    MainTask.postToken(this) {
                        val intent = Intent(this@MainActivity, ScheduleActivity::class.java)
                        startActivity(intent)
                        finish()
                    }.execute()
                } else {
                    SnackbarUtils.error(this, "Usuário inválidos")
                }
            }.execute()
        }

        animation.duration = 1000
        animation.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(p0: Animator?) {

            }
            override fun onAnimationEnd(p0: Animator?) {
                linearLayoutLogin.visibility = View.VISIBLE
                linearLayoutLogin.alpha = 0.toFloat()
                linearLayoutLogin.scaleX = 0.toFloat()
                linearLayoutLogin.scaleY = 0.toFloat()
                linearLayoutLogin.animate()
                        .alpha(1.toFloat())
                        .scaleX(1.toFloat()).scaleY(1.toFloat())
                        .setDuration(300)
                        .start()
            }
            override fun onAnimationCancel(p0: Animator?) {

            }
            override fun onAnimationStart(p0: Animator?) {

            }
        })
        Handler().postDelayed({
            val recordLogin = LoginRepository.getCurrentLogin()
            if (recordLogin != null) {
                if (recordLogin.accessToken != null) {
                    val intent = Intent(this@MainActivity, ScheduleActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
            ConnectionUtils.messageNoConnection(this)
            progressBar.visibility = View.GONE
            animation.start()
        }, 4000)
    }


}
