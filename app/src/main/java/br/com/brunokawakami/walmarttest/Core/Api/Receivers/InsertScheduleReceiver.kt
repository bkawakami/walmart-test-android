package br.com.brunokawakami.walmarttest.Core.Api.Receivers

import com.google.gson.annotations.SerializedName

/**
 * Created by bruno on 6/24/16.
 */
class InsertScheduleReceiver {
    var status: String? = null

    @SerializedName("return")
    var returnData: ScheduleReceiver? = null
}