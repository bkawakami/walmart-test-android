package br.com.brunokawakami.walmarttest.Core.Utils

import android.content.Context
import android.graphics.Color
import com.nispok.snackbar.Snackbar
import com.nispok.snackbar.SnackbarManager

/**
 * Created by bruno on 3/29/16.
 */
object SnackbarUtils {
    fun error(ctx: Context, descr: String) {
        SnackbarManager.show(Snackbar.with(ctx) // context
                .text(descr) // text to display
                .textColor(Color.WHITE).color(ctx.resources.getColor(android.R.color.holo_red_dark)).swipeToDismiss(true).duration(Snackbar.SnackbarDuration.LENGTH_LONG), ctx as android.app.Activity)// action button's ActionClickListener
    }

    fun success(ctx: Context, descr: String) {
        SnackbarManager.show(Snackbar.with(ctx) // context
                .text(descr) // text to display
                .textColor(Color.WHITE).color(ctx.resources.getColor(android.R.color.holo_green_dark)).swipeToDismiss(true).duration(Snackbar.SnackbarDuration.LENGTH_LONG), ctx as android.app.Activity)// action button's ActionClickListener
    }

    fun info(ctx: Context, descr: String) {
        SnackbarManager.show(Snackbar.with(ctx) // context
                .text(descr) // text to display
                .textColor(Color.WHITE).swipeToDismiss(true).duration(Snackbar.SnackbarDuration.LENGTH_LONG), ctx as android.app.Activity)// action button's ActionClickListener
    }
}