package br.com.brunokawakami.walmarttest.Domains.Schedule.Tasks

import android.app.Activity
import android.os.AsyncTask
import br.com.brunokawakami.walmarttest.Core.Api.Base.RestAdapter
import br.com.brunokawakami.walmarttest.Core.Api.Requests.ScheduleRequest
import br.com.brunokawakami.walmarttest.Core.Storage.Entities.ScheduleEntity
import br.com.brunokawakami.walmarttest.Core.Storage.Repositories.ScheduleRepository
import com.orhanobut.logger.Logger
import com.orm.SugarRecord
import com.orm.util.NamingHelper
import java.util.*

/**
 * Created by bruno on 6/27/16.
 */
object ScheduleTasks {
    fun getSchedules(activity: Activity, doneExecute: (List<ScheduleEntity>?) -> Unit) :
        AsyncTask<Void, Void, List<ScheduleEntity>?>{
        return object: AsyncTask<Void, Void, List<ScheduleEntity>?>(){
            override fun doInBackground(vararg p0: Void?): List<ScheduleEntity>? {
                try {
                    SugarRecord.executeQuery(" DELETE FROM "+ NamingHelper.toSQLName(ScheduleEntity::class.java))
                    val baseAdapter = RestAdapter.getOauthAdapter(activity).create(ScheduleRequest::class.java)
                    var listReturn = ArrayList<ScheduleEntity>()
                    baseAdapter.all().execute().body().returnData?.map {
                        listReturn.add(ScheduleRepository.save(it)!!)
                    }
                    return ScheduleRepository.allRecords()
                } catch (e: Exception) {
                    Logger.e(e.message)
                    e.printStackTrace()
                    return null
                }
            }

            override fun onPostExecute(result: List<ScheduleEntity>?) {
                doneExecute(result)
            }

        }
    }

    fun getSearch(activity: Activity, search:String, doneExecute: (List<ScheduleEntity>?) -> Unit) :
            AsyncTask<Void, Void, List<ScheduleEntity>?>{
        return object: AsyncTask<Void, Void, List<ScheduleEntity>?>(){
            override fun doInBackground(vararg p0: Void?): List<ScheduleEntity>? {
                try {
                    SugarRecord.executeQuery(" DELETE FROM "+ NamingHelper.toSQLName(ScheduleEntity::class.java))
                    val baseAdapter = RestAdapter.getOauthAdapter(activity).create(ScheduleRequest::class.java)
                    var listReturn = ArrayList<ScheduleEntity>()
                    baseAdapter.search(search).execute().body().returnData?.map {
                        listReturn.add(ScheduleRepository.save(it)!!)
                    }
                    return ScheduleRepository.allRecords()
                } catch (e: Exception) {
                    Logger.e(e.message)
                    e.printStackTrace()
                    return null
                }
            }

            override fun onPostExecute(result: List<ScheduleEntity>?) {
                doneExecute(result)
            }

        }
    }

    fun insertSchedule(
                activity: Activity,
                begin: String?,
                title: String?,
                content: String?,
                remember: Int?,
                doneExecute: (ScheduleEntity?
            ) -> Unit) :
            AsyncTask<Void, Void, ScheduleEntity?>{
        return object: AsyncTask<Void, Void, ScheduleEntity?>(){
            override fun doInBackground(vararg p0: Void?): ScheduleEntity? {
                try {
                    val baseAdapter = RestAdapter.getOauthAdapter(activity).create(ScheduleRequest::class.java)
                    val saveSchedule = baseAdapter.insert(
                            begin,
                            title,
                            content,
                            remember
                    ).execute().body()

                    return ScheduleRepository.save(saveSchedule.returnData!!)!!
                } catch (e: Exception) {
                    Logger.e(e.message)
                    e.printStackTrace()
                    return null
                }
            }

            override fun onPostExecute(result: ScheduleEntity?) {
                doneExecute(result)
            }

        }
    }

    fun updateSchedule(
            activity: Activity,
            id: Int,
            begin: String?,
            title: String?,
            content: String?,
            remember: Int?,
            doneExecute: (ScheduleEntity?
            ) -> Unit) :
            AsyncTask<Void, Void, ScheduleEntity?>{
        return object: AsyncTask<Void, Void, ScheduleEntity?>(){
            override fun doInBackground(vararg p0: Void?): ScheduleEntity? {
                try {
                    val baseAdapter = RestAdapter.getOauthAdapter(activity).create(ScheduleRequest::class.java)
                    val saveSchedule = baseAdapter.update(
                            id,
                            begin,
                            title,
                            content,
                            remember
                    ).execute().body()

                    return ScheduleRepository.save(saveSchedule.returnData!!)!!
                } catch (e: Exception) {
                    Logger.e(e.message)
                    e.printStackTrace()
                    return null
                }
            }

            override fun onPostExecute(result: ScheduleEntity?) {
                doneExecute(result)
            }

        }
    }

    fun deleteSchedule(
            activity: Activity,
            id: Int,
            doneExecute: () -> Unit) :
            AsyncTask<Void, Void, Unit>{
        return object: AsyncTask<Void, Void, Unit>(){
            override fun doInBackground(vararg p0: Void?): Unit {
                try {
                    val baseAdapter = RestAdapter.getOauthAdapter(activity).create(ScheduleRequest::class.java)
                    baseAdapter.delete(id).execute().body()
                } catch (e: Exception) {
                    Logger.e(e.message)
                    e.printStackTrace()
                }
            }

            override fun onPostExecute(result: Unit) {
                doneExecute()
            }

        }
    }
}