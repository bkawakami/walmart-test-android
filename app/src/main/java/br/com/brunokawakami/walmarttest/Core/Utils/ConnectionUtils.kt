package br.com.brunokawakami.walmarttest.Core.Utils

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import android.provider.Settings
import br.com.brunokawakami.walmarttest.R
import com.nispok.snackbar.Snackbar
import com.nispok.snackbar.SnackbarManager
import com.nispok.snackbar.listeners.ActionClickListener

/**
 * Created by bruno on 3/29/16.
 */
object ConnectionUtils {
    fun isOnline(ctx: Context): Boolean {
        val cm = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    fun messageNoConnection(ctx: Context) {
        if (!isOnline(ctx)) {
            SnackbarManager.show(
                    Snackbar.with(ctx) // context
                            .text("Problemas ao se conectar") // text to display
                            .textColor(Color.WHITE).swipeToDismiss(true).duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE).actionLabel("Ativar") // action button label
                            .actionColorResource(android.R.color.holo_green_dark).actionListener(ActionClickListener {
                        ctx.startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
                        return@ActionClickListener
                    }) // action button's ActionClickListener
                    , ctx as android.app.Activity)
        }
    }
}