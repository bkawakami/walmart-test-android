package br.com.brunokawakami.walmarttest.Core.Utils

import android.content.Context
import com.afollestad.materialdialogs.MaterialDialog

/**
 * Created by bruno on 3/29/16.
 */
object DialogUtils {
    fun progress(ctx: Context, title: String, content: String): MaterialDialog {
        return MaterialDialog.Builder(ctx)
                .title(title)
                .content(content)
                .progress(true, 0)
                .show();
    }

    fun progress(ctx: Context): MaterialDialog{
        return MaterialDialog.Builder(ctx)
                .title("Aguarde")
                .content("Processando")
                .progress(true, 0)
                .show();
    }

}