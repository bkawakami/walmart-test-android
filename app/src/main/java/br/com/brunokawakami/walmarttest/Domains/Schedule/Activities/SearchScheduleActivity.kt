package br.com.brunokawakami.walmarttest.Domains.Schedule.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import br.com.brunokawakami.walmarttest.Domains.Schedule.Fragments.AddScheduleFragment
import br.com.brunokawakami.walmarttest.Domains.Schedule.Fragments.SearchScheduleFragment
import br.com.brunokawakami.walmarttest.R
import net.steamcrafted.materialiconlib.MaterialIconView

class SearchScheduleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_generic_search)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().add(R.id.container, SearchScheduleFragment()).commit()
        }
        val iconBack = findViewById(R.id.iconBack) as MaterialIconView
        iconBack.setOnClickListener {
            finish()
        }
    }
}
