package br.com.brunokawakami.walmarttest.Domains.Schedule.Activities

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import br.com.brunokawakami.walmarttest.Core.Utils.ConnectionUtils
import br.com.brunokawakami.walmarttest.Core.Utils.MenuUtils
import br.com.brunokawakami.walmarttest.Domains.Schedule.Fragments.ScheduleFragment
import br.com.brunokawakami.walmarttest.R
import com.mikepenz.materialdrawer.Drawer
import com.shehabic.droppy.DroppyMenuItem
import com.shehabic.droppy.DroppyMenuPopup
import com.shehabic.droppy.animations.DroppyFadeInAnimation
import net.steamcrafted.materialiconlib.MaterialIconView

class ScheduleActivity : FragmentActivity() {

    var menu: Drawer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_generic)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().add(R.id.container, ScheduleFragment()).commit()
        }
        val iconMenu = findViewById(R.id.iconMenu) as MaterialIconView
        val iconDots = findViewById(R.id.iconDots) as MaterialIconView

        menu = MenuUtils.generate(this)
        iconMenu.setOnClickListener {
            menu?.openDrawer()
        }

        val droppy =  DroppyMenuPopup.Builder(this, iconDots)
        droppy.addMenuItem(DroppyMenuItem("Documentação"))

        droppy.setOnClick { view, i ->
            val urlString="http://walmart.brunokawakami.com.br"
            val intent= Intent(Intent.ACTION_VIEW, Uri.parse(urlString))
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.setPackage("com.android.chrome")
            try {
                startActivity(intent);
            } catch (ex: ActivityNotFoundException) {
                // Chrome browser presumably not installed so allow user to choose instead
                intent.setPackage(null)
                startActivity(intent)
            }
        }
        droppy.setPopupAnimation(DroppyFadeInAnimation())
                .setXOffset(5)
                .setYOffset(5).build()

    }

    override fun onResume() {
        super.onResume()
        menu?.closeDrawer()
        ConnectionUtils.messageNoConnection(this)
    }
}
