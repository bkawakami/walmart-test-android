package br.com.brunokawakami.walmarttest.Core.Api.Requests

import br.com.brunokawakami.walmarttest.Core.Api.Receivers.InsertScheduleReceiver
import br.com.brunokawakami.walmarttest.Core.Api.Receivers.LoginReceiver
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * Created by bruno on 6/24/16.
 */
interface MeRequest {
    @FormUrlEncoded
    @POST("me/token")
    fun token(
            @Field("token") token: String?
    ): Call<InsertScheduleReceiver>
}