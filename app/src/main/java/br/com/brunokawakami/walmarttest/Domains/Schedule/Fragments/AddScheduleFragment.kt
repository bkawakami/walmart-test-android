package br.com.brunokawakami.walmarttest.Domains.Schedule.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import br.com.brunokawakami.walmarttest.Core.Storage.Repositories.ScheduleRepository
import br.com.brunokawakami.walmarttest.Core.Utils.DateUtils
import br.com.brunokawakami.walmarttest.Core.Utils.DialogUtils
import br.com.brunokawakami.walmarttest.Core.Utils.SnackbarUtils
import br.com.brunokawakami.walmarttest.Domains.Schedule.Tasks.ScheduleTasks

import br.com.brunokawakami.walmarttest.R
import com.codetroopers.betterpickers.datepicker.DatePickerBuilder
import com.codetroopers.betterpickers.datepicker.DatePickerDialogFragment
import com.codetroopers.betterpickers.numberpicker.NumberPickerBuilder
import com.codetroopers.betterpickers.numberpicker.NumberPickerDialogFragment
import com.codetroopers.betterpickers.timepicker.TimePickerBuilder
import com.codetroopers.betterpickers.timepicker.TimePickerDialogFragment
import com.orhanobut.logger.Logger
import mehdi.sakout.fancybuttons.FancyButton
import java.math.BigDecimal
import java.math.BigInteger

class AddScheduleFragment : Fragment(),
        DatePickerDialogFragment.DatePickerDialogHandler,
        NumberPickerDialogFragment.NumberPickerDialogHandlerV2,
        TimePickerDialogFragment.TimePickerDialogHandler{

    var textViewRemember: TextView? = null
    var textViewWhen: TextView? = null
    var beginDate: String? = null
    var remember: BigInteger? = BigInteger.valueOf(5)

    override fun onDialogTimeSet(reference: Int, hourOfDay: Int, minute: Int) {
        val hour = if (hourOfDay.toString().length == 1) "0"+hourOfDay.toString() else hourOfDay.toString()
        val minuteFormat = if (minute.toString().length == 1) "0"+minute.toString() else minute.toString()
        textViewWhen?.text =  "${textViewWhen?.text.toString()} $hour:$minuteFormat:00"
        beginDate = "$beginDate $hour:$minuteFormat:00"
    }

    override fun onDialogNumberSet(reference: Int, number: BigInteger?, decimal: Double, isNegative: Boolean, fullNumber: BigDecimal?) {
        Logger.e(number.toString())
        remember = number
        textViewRemember?.text = "Lembrar em ${number.toString()} minutos antes"
    }

    override fun onDialogDateSet(reference: Int, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        val day = if (dayOfMonth.toString().length == 1) "0"+dayOfMonth.toString() else dayOfMonth.toString()
        val month = if (monthOfYear.toString().length == 1) "0"+(monthOfYear+1).toString() else (monthOfYear+1).toString()

        textViewWhen?.text = "Em: $day/$month/${year.toString()}"
        beginDate = "${year.toString()}-$month-$day"

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val rootView = inflater!!.inflate(R.layout.fragment_add_schedule, container, false)
        val scheduleId = activity.intent.getIntExtra("serverId",0)
        val editTextTitle = rootView.findViewById(R.id.editTextTitle) as EditText
        val editTextContent = rootView.findViewById(R.id.editTextContent) as EditText
        val fancyButtonChangeRemember = rootView.findViewById(R.id.fancyButtonChangeRemember) as FancyButton
        val fancyButtonWhenRemember = rootView.findViewById(R.id.fancyButtonWhenRemember) as FancyButton
        val fancyButtonSave = rootView.findViewById(R.id.fancyButtonSave) as FancyButton
        textViewRemember = rootView.findViewById(R.id.textViewRemember) as TextView
        textViewWhen = rootView.findViewById(R.id.textViewWhen) as TextView


        if (scheduleId != 0) {

            val record = ScheduleRepository.find(scheduleId)
            val beginDateString = DateUtils.formatDateToFormat(record?.begin!!, DateUtils.dateFormatDateTime)
            val beginDateFormat = DateUtils.formatDateToFormat(record?.begin!!, DateUtils.dateFormatDayMothYearHourMinuteSecond)

            textViewWhen?.text = beginDateString
            beginDate = beginDateFormat
            if (record!!.timeToRemember != null) {
                remember = BigInteger.valueOf(record.timeToRemember!!.toLong())
                textViewRemember?.text = "Lembrar em ${remember.toString()} minutos antes"
            } else {
                remember = BigInteger.valueOf(0)
                textViewRemember?.text = "Lembrar em ${remember.toString()} minutos antes"
            }
            editTextTitle.text = SpannableStringBuilder(record.title!!)
            editTextContent.text = SpannableStringBuilder(record.content!!)

        }


        fancyButtonWhenRemember.setOnClickListener {
            TimePickerBuilder()
                    .setFragmentManager(childFragmentManager)
                    .setStyleResId(R.style.BetterPickersDialogFragment)
                    .setTargetFragment(this@AddScheduleFragment)
                    .show()
            DatePickerBuilder()
                    .setFragmentManager(childFragmentManager)
                    .setStyleResId(R.style.BetterPickersDialogFragment)
                    .setTargetFragment(this@AddScheduleFragment)
                    .show()
        }

        fancyButtonChangeRemember.setOnClickListener {
            NumberPickerBuilder()
                    .setFragmentManager(childFragmentManager)
                    .setStyleResId(R.style.BetterPickersDialogFragment)
                    .setTargetFragment(this@AddScheduleFragment)
                    .setLabelText("Minutos")
                    .show()

        }

        fancyButtonSave.setOnClickListener {
            val materialDialog = DialogUtils.progress(activity)
            if (scheduleId != 0) {
                ScheduleTasks.updateSchedule(
                        activity,
                        scheduleId,
                        beginDate,
                        editTextTitle.text.toString(),
                        editTextContent.text.toString(),
                        remember?.toInt()
                ) { schedule ->
                    materialDialog.dismiss()
                    if(schedule != null) {
                        SnackbarUtils.success(activity, "Atualizado com sucesso")
                        activity.finish()
                    } else {
                        SnackbarUtils.error(activity, "Erro ao atualizar")
                    }
                }.execute()
            } else {
                ScheduleTasks.insertSchedule(
                        activity,
                        beginDate,
                        editTextTitle.text.toString(),
                        editTextContent.text.toString(),
                        remember?.toInt()
                ) { schedule ->
                    materialDialog.dismiss()
                    if(schedule != null) {
                        SnackbarUtils.success(activity, "Inserido com sucesso")
                        activity.finish()
                    } else {
                        SnackbarUtils.error(activity, "Erro ao inserir")
                    }
                }.execute()
            }
        }


        return rootView
    }
}
