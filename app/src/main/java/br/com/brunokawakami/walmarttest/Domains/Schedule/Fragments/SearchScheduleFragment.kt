package br.com.brunokawakami.walmarttest.Domains.Schedule.Fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import br.com.brunokawakami.walmarttest.Core.Utils.DialogUtils
import br.com.brunokawakami.walmarttest.Domains.Schedule.Activities.AddScheduleActivity
import br.com.brunokawakami.walmarttest.Domains.Schedule.Adapters.ScheduleRecyclerViewAdapter

import br.com.brunokawakami.walmarttest.R
import br.com.brunokawakami.walmarttest.Domains.Schedule.Tasks.ScheduleTasks
import com.afollestad.materialdialogs.MaterialDialog


class SearchScheduleFragment : Fragment() {

    var recyclerViewSchedules: RecyclerView? = null
    var swipeRefreshLayout: SwipeRefreshLayout? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_schedule, container, false)
        recyclerViewSchedules = view.findViewById(R.id.recyclerViewSchedules) as RecyclerView
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout) as SwipeRefreshLayout
        val editTextSearch = activity.findViewById(R.id.editTextSearch) as EditText

        editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                swipeRefreshLayout?.isRefreshing = true
                val searchString = editTextSearch.text.toString()
                updateSchedule(searchString)
            }
        })

        swipeRefreshLayout?.setOnRefreshListener {
            swipeRefreshLayout?.isRefreshing = false
        }

        return view
    }

    private fun updateSchedule(search: String){
        ScheduleTasks.getSearch(activity, search) { listSchedule ->
            if (listSchedule!=null) {
                recyclerViewSchedules?.adapter =
                        ScheduleRecyclerViewAdapter(listSchedule) { schedule ->
                            MaterialDialog.Builder(activity)
                                    .title("Ações")
                                    .items("Editar","Excluír")
                                    .itemsCallback { materialDialog, view, i, charSequence ->
                                        when(i){
                                            0->{
                                                val intent = Intent(activity, AddScheduleActivity::class.java)
                                                intent.putExtra("serverId", schedule.serverId)
                                                activity.startActivity(intent)
                                            }
                                            1->{
                                                val mDiagog = DialogUtils.progress(activity)
                                                ScheduleTasks.deleteSchedule(activity, schedule.serverId!!){
                                                    swipeRefreshLayout?.isRefreshing = true
                                                    updateSchedule(search)
                                                    mDiagog.dismiss()
                                                }.execute()
                                            }
                                        }
                                    }
                                    .show()
                        }
            }
            swipeRefreshLayout?.isRefreshing = false
        }.execute()
    }

}
