package br.com.brunokawakami.walmarttest.Core.Api.Receivers

import com.google.gson.annotations.SerializedName

/**
 * Created by bruno on 6/24/16.
 */
class LoginReceiver {

    @SerializedName("access_token")
    var accessToken: String? = null

    @SerializedName("token_type")
    var tokenType: String? = null

    @SerializedName("expires_in")
    var expiresIn: String? = null

    @SerializedName("refresh_token")
    var refreshToken: String? = null

}