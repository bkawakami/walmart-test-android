package br.com.brunokawakami.walmarttest.Core.Utils

import android.app.Activity
import android.content.Intent
import br.com.brunokawakami.walmarttest.Core.Storage.Entities.LoginEntity
import br.com.brunokawakami.walmarttest.Core.Storage.Entities.ScheduleEntity
import br.com.brunokawakami.walmarttest.Core.Storage.Repositories.LoginRepository
import br.com.brunokawakami.walmarttest.Domains.Main.Activities.MainActivity
import br.com.brunokawakami.walmarttest.Domains.Schedule.Activities.AddScheduleActivity
import br.com.brunokawakami.walmarttest.Domains.Schedule.Activities.ScheduleActivity
import br.com.brunokawakami.walmarttest.Domains.Schedule.Activities.SearchScheduleActivity
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.DividerDrawerItem
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.orhanobut.logger.Logger
import com.orm.SugarRecord
import com.orm.util.NamingHelper

/**
 * Created by bruno on 6/27/16.
 */
object MenuUtils {
    fun generate(activity: Activity): Drawer {

//val login = LoginRepository.getCurrentLogin()
//        val profile = ProfileDrawerItem()
//                .withName(login?.name)
//                .withEmail(login?.email)
//                .withIcon(login?.picture)
//
//        val headerResult = AccountHeaderBuilder()
//                .withActivity(activity)
//                .withHeaderBackground(R.drawable.header_profile)
//                .addProfiles(profile)
//                .build()


        val menuList = arrayOf(
                PrimaryDrawerItem().withName("Buscar"),
                DividerDrawerItem(),
                PrimaryDrawerItem().withName("Adicionar"),
                DividerDrawerItem(),
                PrimaryDrawerItem().withName("Logout")
        )

        val result = DrawerBuilder()
                .withActivity(activity)
                .withTranslucentStatusBar(false)
                .withActionBarDrawerToggle(true)
                .withSelectedItem(-1)
                .addDrawerItems(*menuList)
                .withOnDrawerItemClickListener({ view, position, drawerItem ->
                    Logger.e(position.toString())
                    var intent: Intent
                    when (position){
                        0 -> {
                            intent = Intent(activity, SearchScheduleActivity::class.java)
                        }
                        2 -> {
                            intent = Intent(activity, AddScheduleActivity::class.java)
                        }
                        4 -> {
                            intent = Intent(activity, MainActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            SugarRecord.executeQuery(" DELETE FROM "+ NamingHelper.toSQLName(ScheduleEntity::class.java))
                            val loginRecord = LoginRepository.getCurrentLogin()
                            loginRecord?.accessToken = null
                            loginRecord?.refreshToken = null
                            loginRecord?.save()
                        }
                        else -> {intent = Intent(activity, ScheduleActivity::class.java) }
                    }
                    activity.startActivity(intent)
                    true
                }).build()

        return result
    }
}