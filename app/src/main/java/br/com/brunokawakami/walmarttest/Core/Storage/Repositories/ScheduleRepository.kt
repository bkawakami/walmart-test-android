package br.com.brunokawakami.walmarttest.Core.Storage.Repositories

import br.com.brunokawakami.walmarttest.Core.Api.Receivers.LoginReceiver
import br.com.brunokawakami.walmarttest.Core.Api.Receivers.ScheduleReceiver
import br.com.brunokawakami.walmarttest.Core.Storage.Entities.LoginEntity
import br.com.brunokawakami.walmarttest.Core.Storage.Entities.ScheduleEntity
import br.com.brunokawakami.walmarttest.Core.Utils.DateUtils
import com.orm.query.Condition
import com.orm.query.Select

/**
 * Created by bruno on 6/24/16.
 */
object ScheduleRepository {
    fun find(id: Int): ScheduleEntity?{
        return Select.from(ScheduleEntity::class.java)
                .where(Condition.prop("server_id").eq(id))
                .first()
    }

    fun allRecords(): List<ScheduleEntity>?{
        return Select.from(ScheduleEntity::class.java)
                .orderBy("begin ASC")
                .list()
    }

    fun save(scheduleReceiver: ScheduleReceiver): ScheduleEntity?{
        var record = find(scheduleReceiver.id!!)
        val dateBegin = DateUtils.convertStringToDate(scheduleReceiver.begin!!)
        if (record == null) {
            record = ScheduleEntity(
                    scheduleReceiver.id!!,
                    scheduleReceiver.title,
                    scheduleReceiver.content,
                    dateBegin,
                    scheduleReceiver.timeToRemember
            )
            record.save()
            return record
        } else {
            record.begin = dateBegin
            record.content = scheduleReceiver.content
            record.timeToRemember = scheduleReceiver.timeToRemember
            record.title = scheduleReceiver.title
            record.save()
        }
        return record
    }
}